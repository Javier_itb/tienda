<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Bombones',
            'precio' => 7.80,
            'stock' => 7,
            'description' => 'Surtido de bombones, 250gr.',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/img/alimentos.jpeg')),
        ]);
        DB::table('products')->insert([
            'name' => 'Aquarius',
            'precio' => 0.65,
            'stock' => 75,
            'description' => 'Lata aquarius 33cl.',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/img/alimentos.jpeg')),
        ]);
    }
}
