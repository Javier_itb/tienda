<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Product;
use Illuminate\Support\Facades\Log;


class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function prueba(Request $request) {

        //$products = Product::all;
        $products = DB::table('products')
            ->join('cart_items', 'products.id', '=','cart_items.product_id')
            ->join('cart','cart.id','=','cart_items.cart_id')
            ->select('users.*','cart_items.quantity')->get();

        return view('products')->with('products',$products);
    }

    public function show(Request $request) {
        $valueMin = empty($request->input('priceMin'))?'0':$request->input('priceMin') ;
        $valueMax = empty($request->input('priceMax'))?'9999':$request->input('priceMax') ;

        $all = DB::select(DB::raw("select * from products where precio >= $valueMin and precio <= $valueMax"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 8;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products',$products);
    }

    public function addToCart($id) {
        $product = Product::find($id);

        Log::error('Error' . $id);
        Log::warning('Warning' . $id);
        Log::notice('Notice' . $id);
        Log::info('Info' . $id);
        Log::debug('Debug' . $id);

        Log::channel('buy')->info('Ejemplo log info');

        if(!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        $user = Auth::user()->id;
        $cart_bd = Cart::where('user_id', $user)->first();

        if (empty($cart_bd)) {
            $cart_bd = new Cart();
            $cart_bd->user_id = $user;
            $cart_bd->save();
        }

        if(!$cart) {
            $cart = [
                $id => [
                    "name" => $product->name,
                    "quantity" => 1,
                    "price" => $product->precio,
                    "image" => $product->image
                ]
            ];
            $cart_item = new Cart_Item();
            $cart_item->cart_id = $cart_bd->id;
            $cart_item->product_id = $id;
            $cart_item->quantity = 1;
            $cart_item->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
            $cart_item = Cart_Item::where([['cart_id', $cart_bd->id],['product_id', $id]])->first();
            //dd($cart_item);
            $cart_item->quantity = $cart[$id]['quantity'];
            $cart_item->update();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "name" => $product->name,
            "quantity" => 1,
            "price" => $product->precio,
            "image" => $product->image
        ];
        $cart_item = new Cart_Item();
        $cart_item->cart_id = $cart_bd->id;
        $cart_item->product_id = $id;
        $cart_item->quantity = 1;
        $cart_item->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function cart() {

        //recuperar usuario
        $user = Auth::user()->id;
        //recuperar carrito
        $cart_bd = Cart::where('user_id', $user)->first();
        //Si no tiene carrito creamos uno
        if (($cart_bd) == null) {
            $cart_bd = new Cart();
            $cart_bd->user_id = $user;
            $cart_bd->save();
        }
        $cart = [];
        //recuperar cart_item
        $cart_item = Cart_Item::where('cart_id', $cart_bd->id)->get();
        //dd($cart_item);

        //un for each de cart_item
        //rellenar cart con product y cart_item
        foreach ($cart_item as $item) {
            $product_id = $item->product_id;
            $product = Product::find($product_id);
            //dd($product);
            $cart[$product_id] = [
                "name" => $product->name,
                "quantity" => $item->quantity,
                "price" => $product->precio,
                "image" => $product->image
            ];
        }

        session()->put('cart', $cart);
        return view('cart');
    }

    public function update(Request $request) {

        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;

            $user = Auth::user()->id;
            $cart_bd = Cart::where('user_id', $user)->first();
            $cart_item = Cart_Item::where([['cart_id', $cart_bd->id],['product_id', $request->id]])->first();
            $cart_item->quantity = $request->quantity;
            $cart_item->update();

            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request) {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_bd = Cart::where('user_id', $user)->first();
                $cart_item = Cart_Item::where([['cart_id', $cart_bd->id],['product_id', $request->id]])->first();
                $cart_item->delete();

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully!');
        }
    }

    public function venta() {
        $user = Auth::user()->id;

        $cart_bd = Cart::where('user_id', $user)->first();

        $cart_item = Cart_Item::where('cart_id', $cart_bd->id)->get();

        $ok = true;

        foreach ($cart_item as $item) {
            $product_id = $item->product_id;
            $product = Product::find($product_id);

            $quantity = $item->quantity;
            $stock = $product->stock;

            if ($quantity > $stock) {
                $ok = false;
                session()->flash('success', 'Stock insuficiente en ' . $product->name . ". Quedan " . $stock);
            }
        }
        if ($ok) {

            //Finalizar compra
            foreach ($cart_item as $item) {
                $product_id = $item->product_id;
                $product = Product::find($product_id);

                $quantity = $item->quantity;
                $stock = $product->stock;

                $stock_final = $stock-$quantity;

                DB::update("UPDATE products set stock = $stock_final where id = $product_id");
                //Vaciar carrito
                DB::delete("DELETE FROM cart_items where cart_id = $cart_bd->id");
                session()->flash('success', 'Compra realizada con éxito. ¡Gracias por su compra!');
            }
        }
    }
}
