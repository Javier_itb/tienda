@extends('layouts.app')

@section('content')

    <div style="display: flex; justify-content: center; text-align: center; font-size: 20px">
        <p style="background-image: url(https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1200px-Flag_of_the_United_Kingdom.svg.png);
        background-size: 50px 30px; background-repeat: no-repeat; text-shadow: 0 0 2px #ffffff; width: 50px; margin: 0 10px 0 10px">
            <a href="{{ url('lang', ['en']) }}">EN</a></p>

        <p style="background-image: url(https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Spain_%28Civil%29.svg/1280px-Flag_of_Spain_%28Civil%29.svg.png);
        background-size: 50px 30px; background-repeat: no-repeat; width: 50px; margin: 0 10px 0 10px">
            <a href="{{ url('lang', ['es']) }}">ES</a></p>

        <p style="background-image: url(https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Flag_of_Catalonia.svg/1200px-Flag_of_Catalonia.svg.png);
        background-size: 50px 30px; background-repeat: no-repeat; width: 50px; margin: 0 10px 0 10px">
            <a href="{{ url('lang', ['ca']) }}">CA</a></p>
    </div>


    <div style="padding: 5px; background-color: #abcbe9;" class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style=" max-width: 1200px; margin-left: auto; margin-right: auto">
                    <button onclick="window.location.href='/cart'" style="background-image: url(https://image.flaticon.com/icons/png/512/263/263142.png); width: 50px; height: 50px; background-position: center; background-size: cover; margin-left: auto; margin-right: auto; display: block" ></button>
                    <div style="font-size: 24px; text-align: center" class="card-header"> -- {{ __('messages.Products') }} --
                        <form method="get" action="/products">
                            <input name="priceMin" type="text" placeholder="{{ __('messages.minPrice') }}">
                            <input name="priceMax" type="text" placeholder="{{ __('messages.maxPrice') }}">
                            <input name="buscar" type="submit" value="{{ __('messages.Search') }}" style="margin-top: 5px; margin-left: 5px; padding-left: 10px; padding-right: 10px">
                        </form>
                    </div>
                    <div class="card-body" style="display: flex; flex-wrap: wrap; justify-content: center">
                        @foreach ($products as $product)
                            <div style="padding: 30px">
                                <img style="width: 220px; margin-bottom: 15px" src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                                <div style="font-size: 16px; max-width: 220px; font-weight: bold">{{$product->name}}, {{$product->precio}}€</div>
                                <div style="font-size: 14px; max-width: 220px; margin-bottom: 10px">{{$product->description}}</div>
                                <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">-> {{ __('messages.Add') }}</a> </p>
                            </div>
                        @endforeach

                    </div>
                    <div style="display: flex; justify-content: center">{{ $products->links() }}</div>
                </div> </div> </div> </div>@endsection


