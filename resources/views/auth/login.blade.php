<x-guest-layout>

    <div style="display: flex; justify-content: center; text-align: center; font-size: 20px">
        <p style="background-image: url(https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1200px-Flag_of_the_United_Kingdom.svg.png);
        background-size: 50px 30px; background-repeat: no-repeat; text-shadow: 0 0 2px #ffffff; width: 50px; margin: 0 10px 0 10px">
            <a href="{{ url('lang', ['en']) }}">EN</a></p>

        <p style="background-image: url(https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Spain_%28Civil%29.svg/1280px-Flag_of_Spain_%28Civil%29.svg.png);
        background-size: 50px 30px; background-repeat: no-repeat; width: 50px; margin: 0 10px 0 10px">
            <a href="{{ url('lang', ['es']) }}">ES</a></p>

        <p style="background-image: url(https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Flag_of_Catalonia.svg/1200px-Flag_of_Catalonia.svg.png);
        background-size: 50px 30px; background-repeat: no-repeat; width: 50px; margin: 0 10px 0 10px">
            <a href="{{ url('lang', ['ca']) }}">CA</a></p>
    </div>

    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('messages.Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('messages.Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('messages.Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('messages.Login') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
