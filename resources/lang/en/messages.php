<?php

return [
    'Name' => 'Name',
    'Surname' => 'Surname',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm Password',
    'Already registered?' => 'Already registered?',
    'Register' => 'Register',
    'Remember me' => 'Remember me',
    'Forgot your password?' => 'Forgot your password?',
    'Login' => 'Login',
    'Products' => 'Products',
    "Search" => 'Search',
    "Add" => 'add to cart',
    "minPrice" => 'cost grater than...',
    "maxPrice" => 'and lower than...',
];
