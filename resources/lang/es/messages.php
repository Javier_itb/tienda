<?php

return [
    'Name' => 'Nombre',
    'Surname' => 'Apellido',
    'Password' => 'Contraseña',
    'Confirm Password' => 'Confirmar Contraseña',
    'Already registered?' => '¿Ya estás registrado?',
    'Register' => 'Registrarse',
    'Remember me' => 'Recuerdame',
    'Forgot your password?' => '¿Te has olvidado la contraseña?',
    'Login' => 'Iniciar sesión',
    'Products' => 'Productos',
    "Search" => 'Busca',
    "Add" => 'añadir al carrito',
    "minPrice" => 'coste superior a...',
    "maxPrice" => 'y menor a...',
];

