<?php

return [
    'Name' => 'Nom',
    'Surname' => 'Cognom',
    'Password' => 'Contrasenya',
    'Confirm Password' => 'Confirmar Contrasenya',
    'Already registered?' => 'Ja estàs registrat?',
    'Register' => 'Registrar-se',
    'Remember me' => 'Recordar-me',
    'Forgot your password?' => 'T\'has oblidat la contrasenya?',
    'Login' => 'Inicia sessió',
    'Products' => 'Productes',
    "Search" => 'Cerca',
    "Add" => 'afegeix al carret',
    "minPrice" => 'cost superior a...',
    "maxPrice" => 'i inferior a...',
];
